package com.example.demo.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import com.example.demo.dao.PropertyDao;
import com.example.demo.model.Property;
import com.example.demo.util.Page4Navigator;

@Service
public class PropertyService {
    @Autowired PropertyDao propertyDao;
 
    public List<Property> list() {
        return propertyDao.findAll();
    }
    
    public void add(Property bean) {
        propertyDao.save(bean);
    }
    
    public Property get(Integer id) {
     return   propertyDao.getOne(id);
    }
    
    public void update(Property bean) {
    	propertyDao.saveAndFlush(bean);
    }
    
    public Page4Navigator<Property> list(int start, int size, int navigatePages) {
        
        Sort sort =Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(start, size, sort);
 
        Page pageFromJPA =propertyDao.findAll(pageable);
        
        return new Page4Navigator<>(pageFromJPA,navigatePages);
    
    }
    
}