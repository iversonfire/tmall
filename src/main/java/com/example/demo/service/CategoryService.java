package com.example.demo.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import com.example.demo.dao.CategoryDao;
import com.example.demo.model.Category;
import com.example.demo.util.Page4Navigator;

@Service
public class CategoryService {
    @Autowired CategoryDao categoryDao;
 
    public List<Category> list() {
        return categoryDao.findAll();
    }
    
    public void add(Category bean) {
        categoryDao.save(bean);
    }
    
    public Category get(Integer id) {
     return   categoryDao.getOne(id);
    }
    
    public void update(Category bean) {
    	categoryDao.saveAndFlush(bean);
    }
    
    public Page4Navigator<Category> list(int start, int size, int navigatePages) {
        
        Sort sort =Sort.by(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(start, size, sort);
 
        Page pageFromJPA =categoryDao.findAll(pageable);
        
        return new Page4Navigator<>(pageFromJPA,navigatePages);
    
    }
    
}