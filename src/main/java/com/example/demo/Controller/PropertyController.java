package com.example.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.dao.ProductDao;
import com.example.demo.model.Product;
import com.example.demo.model.Property;
import com.example.demo.service.PropertyService;

@Controller
public class PropertyController  {

@Autowired 
PropertyService propertyService;


@GetMapping("/properties/{cid}")
public List<Property> list(@PathVariable Integer cid){
	List list =propertyService.list();
	return list;
}


}