package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Property;
@Repository("propertyDao")
public interface PropertyDao extends JpaRepository<Property,Integer>{
		 
}